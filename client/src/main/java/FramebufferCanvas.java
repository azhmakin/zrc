import framebuffer.CanvasProxy;
import framebuffer.ClientFramebuffer;
import messages.KeyEventMessage;
import messages.PointerEventMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class FramebufferCanvas
        extends JPanel
        implements MouseMotionListener,
                   MouseListener,
                   KeyListener
{
    private CanvasProxy canvasProxy;
    private ClientFramebuffer framebuffer;
    private BufferedImage frame;
    private DataOutputStream outputStream;
    private ClientThread clientThread;


    public FramebufferCanvas()
    {
        super();

        this.setBackground( Color.WHITE );
        this.canvasProxy = new MyCanvasProxy();
    }


    public void paintComponent( Graphics graphicsContext )
    {
        super.paintComponent( graphicsContext );

        graphicsContext.drawImage( this.frame, 0, 0, null );
    }


    public void setFramebuffer( ClientFramebuffer framebuffer )
    {
        this.framebuffer = framebuffer;

        this.frame = new BufferedImage( this.framebuffer.getWidth(),
                                        this.framebuffer.getHeight(),
                                        BufferedImage.TYPE_INT_ARGB );
    }


    public void setOutputStream( DataOutputStream outputStream )
    {
        this.outputStream = outputStream;

        this.setFocusable( true );
        this.requestFocusInWindow();

        this.addMouseListener( this );
        this.addMouseMotionListener( this );
        this.addKeyListener( this );
    }


    public void deactivate()
    {
        this.removeMouseListener( this );
        this.removeMouseMotionListener( this );
        this.removeKeyListener( this );
    }


    public void paintBlack()
    {
        Graphics2D g = this.frame.createGraphics();

        g.setColor( Color.WHITE );
        g.fillRect( 0, 0, this.frame.getWidth(), this.frame.getHeight() );

        this.repaint();
    }


    public CanvasProxy getCanvasProxy()
    {
        return this.canvasProxy;
    }


    public void setClientThread( ClientThread clientThread )
    {
        this.clientThread = clientThread;
    }


    @Override
    public void keyTyped( KeyEvent e )
    {
        // Do nothing?
    }


    @Override
    public void keyPressed( KeyEvent e )
    {
        while ( this.clientThread.isOutputLocked() )
            ;

        this.clientThread.lockOutput();

        KeyEventMessage message = new KeyEventMessage();

        message.setKey( e.getKeyCode() );
        message.pressed = true;

        try
        {
            message.write( this.outputStream );
        }
        catch ( IOException e1 )
        {
            e1.printStackTrace();
        }

        this.clientThread.unlockOutput();
    }

    @Override
    public void keyReleased( KeyEvent e )
    {
        while ( this.clientThread.isOutputLocked() )
            ;

        this.clientThread.lockOutput();

        KeyEventMessage message = new KeyEventMessage();

        message.setKey( e.getKeyCode() );
        message.pressed = false;

        try
        {
            message.write( this.outputStream );
        }
        catch ( IOException e1 )
        {
            e1.printStackTrace();
        }

        this.clientThread.unlockOutput();
    }

    @Override
    public void mouseClicked( MouseEvent e )
    {
        while ( this.clientThread.isOutputLocked() )
            ;

        this.clientThread.lockOutput();

        PointerEventMessage message = new PointerEventMessage();

        message.button = 1;

        message.x = e.getX();
        message.y = e.getY();

        try
        {
            message.write( this.outputStream );
        }
        catch ( IOException e1 )
        {
            e1.printStackTrace();
        }

        this.clientThread.unlockOutput();
    }

    @Override
    public void mousePressed( MouseEvent e )
    {
        // Do nothing?
    }

    @Override
    public void mouseReleased( MouseEvent e )
    {
        // Do nothing?
    }

    @Override
    public void mouseEntered( MouseEvent e )
    {
        // Do nothing?
    }

    @Override
    public void mouseExited( MouseEvent e )
    {
        // Do nothing?
    }

    @Override
    public void mouseDragged( MouseEvent e )
    {
        // Do nothing?
    }

    @Override
    public void mouseMoved( MouseEvent e )
    {
        while ( this.clientThread.isOutputLocked() )
            ;

        this.clientThread.lockOutput();

        PointerEventMessage message = new PointerEventMessage();

        message.button = 0;

        message.x = e.getX();
        message.y = e.getY();

        try
        {
            message.write( this.outputStream );
        }
        catch ( IOException e1 )
        {
            e1.printStackTrace();
        }

        this.clientThread.unlockOutput();
    }


    private final class MyCanvasProxy implements CanvasProxy
    {
        @Override
        public void plot( int x, int y, int color )
        {
            FramebufferCanvas.this.frame.setRGB( x, y, color );
        }

        @Override
        public void flush()
        {
            FramebufferCanvas.this.repaint();
        }
    }
}
