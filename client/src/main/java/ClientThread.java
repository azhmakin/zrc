import framebuffer.ClientFramebuffer;
import messageprocessors.FramebufferUpdateProcessor;
import messageprocessors.MessageProcessor;
import messageprocessors.SetColourMapEntriesProcessor;
import messages.*;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class ClientThread extends Thread
{
    private String serverAddress;
    private Integer serverPort;
    private MessageProcessor[] messageProcessor;
    private ClientFramebuffer framebuffer;
    private FramebufferCanvas framebufferCanvas;
    private Socket socket;
    private boolean inMessageLoop;
    private DataOutputStream outputStream;
    private DataInputStream inputStream;
    private boolean bitOutputLocked;

    private TimerTask updateRequestTimerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                if ( !isOutputLocked() )
                {
                    try
                    {
                        requestFramebufferUpdate();
                    }
                    catch ( IOException e )
                    {
                        inMessageLoop = false;
                        e.printStackTrace();
                    }
                }
            }
        };


    public ClientThread( String serverAddress, Integer serverPort, FramebufferCanvas framebufferCanvas )
    {
        this.serverAddress = serverAddress;
        this.serverPort = (serverPort != null) ? serverPort : RCUtil.DEFAULT_LISTENING_PORT;
        this.framebufferCanvas = framebufferCanvas;
    }


    public void run()
    {
        try
        {
            //System.out.println( "Start client thread" );

            int port = RCUtil.DEFAULT_LISTENING_PORT;

            this.socket = new Socket( this.serverAddress, port );

            this.outputStream = new DataOutputStream( this.socket.getOutputStream() );
            this.inputStream = new DataInputStream( this.socket.getInputStream() );

            // Read protocol version message
            String serverProtocolVersionStr = this.inputStream.readLine();
            byte[] serverProtocolVersion = serverProtocolVersionStr.getBytes();

            //System.out.println("Protocol version received");

            if ( !RCUtil.compareProtocolVersion( RCUtil.PROTOCOL_VERSION, serverProtocolVersion ) )
            {
                // TODO : Implement me!
                assert false : "Protocol version does not match!";
                return;
            }

            this.outputStream.write( RCUtil.PROTOCOL_VERSION );

            SecurityTypeMessage securityTypeMessage = new SecurityTypeMessage();
            securityTypeMessage.read( this.inputStream );

            int securityType = 0;
            for ( ; securityType < securityTypeMessage.type.length; securityType++ )
            {
                securityType = securityTypeMessage.type[securityType];

                if ( securityType == 1 )
                {
                    this.outputStream.writeByte( 1 );
                    break;
                }
                else if ( securityType == 2 )
                {
                    this.outputStream.writeByte( 2 );
                    break;
                }
            }

            //System.out.println( "securityType = " + securityType );

            // No security type chosen
            if ( securityType == securityTypeMessage.type.length )
            {
                this.outputStream.writeByte( 0 );

                String msg = "Client doesn't support any of these security types!";

                System.out.println( msg );

                this.outputStream.writeInt( msg.length() );
                this.outputStream.write( msg.getBytes() );

                this.socket.close();
                return;
            }

            //int securityResult = inputStream.readByte();
            //assert securityResult == 0;

            //System.out.println( "securityResult = " + securityResult );

            // Send ClientInit message
            // 1 - leave other clients
            this.outputStream.writeByte( 1 );

            // Read ServerInit message
            ServerInitMessage serverInitMessage = new ServerInitMessage();
            serverInitMessage.read( inputStream );
            //System.out.println( "ServerInit received" );

            // TODO : Set up framebuffer!
            this.framebuffer = new ClientFramebuffer( this.framebufferCanvas.getCanvasProxy() );
            this.framebuffer.setSize( serverInitMessage.framebufferWidth, serverInitMessage.framebufferHeight );
            this.framebufferCanvas.setFramebuffer( this.framebuffer );
            this.framebufferCanvas.setOutputStream( this.outputStream );
            this.framebufferCanvas.setClientThread( this );

            //System.out.println("Framebuffer set!");

            this.initMessageProcessors();

            //System.out.println("Message processors initialized!");

            // Send a dummy set pixel format message to trigger sending color map
            SetPixelFormatMessage setPixelFormatMessage = new SetPixelFormatMessage();
            setPixelFormatMessage.write( this.outputStream );

            //System.out.println("Pixel format request sent!");

            this.requestFramebufferUpdate();

            Timer updateRequestTimer = new Timer();
            updateRequestTimer.schedule( this.updateRequestTimerTask, 5000L, 1000L );

            this.inMessageLoop = true;
            this.bitOutputLocked = false;

            // TODO: Message loop!
            while ( this.inMessageLoop )
            {
                byte id = this.inputStream.readByte();
                //System.out.println( "id = " + id );

                MessageProcessor processor = this.messageProcessor[id];
                assert processor != null;

                this.reactOnMessage( processor );
            }

            updateRequestTimer.cancel();
            this.socket.close();


            //System.out.println("Client : Connection successfully closed!");
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        catch ( AWTException e )
        {
            e.printStackTrace();
        }
    }


    synchronized void lockOutput()
    {
        this.bitOutputLocked = true;
    }


    synchronized void unlockOutput()
    {
        this.bitOutputLocked = false;
    }


    synchronized boolean isOutputLocked()
    {
        return this.bitOutputLocked && this.inMessageLoop;
    }


    private void reactOnMessage( MessageProcessor processor )
            throws IOException
    {
        while ( this.isOutputLocked() )
            ;

        this.lockOutput();

        processor.react( this.inputStream );

        this.unlockOutput();
    }


    private void initMessageProcessors()
    {
        this.messageProcessor = new MessageProcessor[4];


        FramebufferUpdateProcessor framebufferUpdateProcessor
                = new FramebufferUpdateProcessor( new FramebufferUpdateMessage(),
                                                  this.framebuffer );

        this.messageProcessor[framebufferUpdateProcessor.getID()] = framebufferUpdateProcessor;


        SetColourMapEntriesProcessor setColourMapEntriesProcessor
                = new SetColourMapEntriesProcessor( new SetColourMapEntriesMessage(),
                                                    this.framebuffer );

        this.messageProcessor[setColourMapEntriesProcessor.getID()] = setColourMapEntriesProcessor;
    }


    private void requestFramebufferUpdate()
            throws IOException
    {
        this.lockOutput();

        FramebufferUpdateRequestMessage framebufferUpdateRequestMessage = new FramebufferUpdateRequestMessage();

        framebufferUpdateRequestMessage.x = 0;
        framebufferUpdateRequestMessage.y = 0;

        framebufferUpdateRequestMessage.width = Math.min( this.framebuffer.getWidth(),
                this.framebufferCanvas.getWidth() );

        framebufferUpdateRequestMessage.height = Math.min( this.framebuffer.getHeight(),
                this.framebufferCanvas.getHeight() );

        framebufferUpdateRequestMessage.write( this.outputStream );

        this.unlockOutput();
    }


    public void disconnect()
    {
        this.inMessageLoop = false;
    }
}
