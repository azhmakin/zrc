package messageprocessors;

import framebuffer.ClientFramebuffer;
import messages.Message;
import messages.SetColourMapEntriesMessage;

import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetColourMapEntriesProcessor extends MessageProcessor
{
    public SetColourMapEntriesProcessor( Message message, ClientFramebuffer framebuffer )
    {
        super( message, framebuffer );
    }


    @Override
    protected void reactBody()
            throws IOException
    {
        ClientFramebuffer framebuffer = (ClientFramebuffer) this.framebuffer;

        SetColourMapEntriesMessage message = (SetColourMapEntriesMessage) this.message;

        int i = message.firstColor;

        for ( SetColourMapEntriesMessage.Entry entry : message.entryList )
        {
            framebuffer.colorTable[i++] = entry.toRGB();
        }
    }
}
