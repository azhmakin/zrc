package messageprocessors;

import framebuffer.ClientFramebuffer;
import messages.FramebufferUpdateMessage;

import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class FramebufferUpdateProcessor extends MessageProcessor
{

    public FramebufferUpdateProcessor( FramebufferUpdateMessage message, ClientFramebuffer framebuffer )
    {
        super( message, framebuffer );

        message.framebuffer = framebuffer;
    }


    @Override
    protected void reactBody()
            throws IOException
    {
        // Do nothing?
    }
}
