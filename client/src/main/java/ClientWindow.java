import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.net.URL;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class ClientWindow extends JFrame
{
    private ClientThread clientThread;

    private JButton cmdConnect;
    private JButton cmdDisconnect;
    private FramebufferCanvas pnlFramebufferCanvas;


    public ClientWindow()
    {
        super();

        initUI();

        this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

        this.setTitle( "ZRC Client - Andrey Zhmakin, 2013" );

        this.setSize( 800, 600 );
        this.setLocationRelativeTo( null );

        this.setVisible( true );
    }


    private void initUI()
    {
        Container contentPane = this.getContentPane();

        // Create toolbar
        JToolBar toolBar = new JToolBar( "Toolbar" );

        this.cmdConnect = this.makeToolbarButton( "icons/connect.png", "Connect" );
        toolBar.add( this.cmdConnect );
        this.cmdConnect.addActionListener( new AbstractAction()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                actionConnect();
            }
        } );

        this.cmdDisconnect = this.makeToolbarButton( "icons/disconnect.png", "Disconnect" );
        toolBar.add( this.cmdDisconnect );
        this.cmdDisconnect.addActionListener( new AbstractAction()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                actionDisconnect();
            }
        } );

        this.cmdDisconnect.setEnabled( false );

        this.add( toolBar, BorderLayout.PAGE_START );

        // Create canvas
        this.pnlFramebufferCanvas = new FramebufferCanvas();
        contentPane.add( this.pnlFramebufferCanvas, BorderLayout.CENTER );
    }



    private JButton makeToolbarButton( String iconResource,
                                       String tooltip )
    {
        URL iconURL = ClientWindow.class.getResource( iconResource );

        Icon icon = new ImageIcon(iconURL);

        JButton button = new JButton( icon );

        button.setToolTipText( tooltip );

        return button;
    }


    private void actionConnect()
    {
        String serverAddress = JOptionPane.showInputDialog( this, "Server: " );

        if ( serverAddress != null )
        {
            Object[] parsed = parseAddress(serverAddress);

            this.clientThread = new ClientThread( (String) parsed[0], (Integer) parsed[1], this.pnlFramebufferCanvas );
            this.clientThread.start();

            this.cmdConnect.setEnabled( false );
            this.cmdDisconnect.setEnabled( true );
        }
    }


    private void actionDisconnect()
    {
        this.pnlFramebufferCanvas.deactivate();

        this.clientThread.disconnect();
        //this.clientThread.stop();

        while ( this.clientThread.isAlive() )
            ;

        this.pnlFramebufferCanvas.paintBlack();

        this.cmdConnect.setEnabled( true );
        this.cmdDisconnect.setEnabled( false );
    }


    private static String invert(@NotNull String s)
    {
        return new StringBuilder(s).reverse().toString();
    }


    private static Object[] parseAddress(String string)
    {
        String inverted = invert(string);

        int colonIndex = inverted.indexOf(':');

        if (colonIndex == -1)
        {
            return new Object[] { string, null };
        }

        String port = invert(inverted.substring(0, colonIndex));
        String addr = invert(inverted.substring(colonIndex + 1));

        Object[] result = new Object[] { null, null };

        try
        {
            result[1] = Integer.parseInt(port);
            result[0] = addr;
        }
        catch (NumberFormatException e)
        {
            result[0] = string;
        }

        return result;
    }
}
