package messageprocessors;

import framebuffer.ServerFramebuffer;
import messages.SetColourMapEntriesMessage;
import messages.SetPixelFormatMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetPixelFormatProcessor extends MessageProcessor
{
    private DataOutputStream outputStream;


    public SetPixelFormatProcessor( SetPixelFormatMessage message,
                                    ServerFramebuffer   framebuffer,
                                    DataOutputStream    outputStream )
    {
        super( message, framebuffer );

        this.outputStream = outputStream;
    }


    @Override
    protected void reactBody() throws IOException
    {
        this.initColorMap( this.outputStream );
    }


    private void initColorMap( DataOutputStream stream ) throws IOException
    {
        SetColourMapEntriesMessage setColourMapEntries = new SetColourMapEntriesMessage();

        setColourMapEntries.entryList = new ArrayList<SetColourMapEntriesMessage.Entry>( 256 );

        for ( int i = 0; i < 256; i++ )
        {
            short j = (short)(i << 8);
            setColourMapEntries.entryList.add( new SetColourMapEntriesMessage.Entry( j, j, j ) );
        }

        setColourMapEntries.write( stream );
    }
}
