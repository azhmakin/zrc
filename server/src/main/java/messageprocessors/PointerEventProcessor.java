package messageprocessors;

import framebuffer.ServerFramebuffer;
import messages.PointerEventMessage;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class PointerEventProcessor extends MessageProcessor
{
    public PointerEventProcessor( PointerEventMessage message, ServerFramebuffer framebuffer )
    {
        super( message, framebuffer );
    }


    @Override
    public void reactBody()
    {
        PointerEventMessage pointerEvent = (PointerEventMessage) this.message;

        ServerFramebuffer framebuffer = (ServerFramebuffer) this.framebuffer;

        framebuffer.mouseTo( pointerEvent.x, pointerEvent.y );

        framebuffer.mouseLeftButton( ( (pointerEvent.button & 1) != 0 ) );

        framebuffer.mouseRightButton( ( (pointerEvent.button & 2) != 0 ) );
    }
}
