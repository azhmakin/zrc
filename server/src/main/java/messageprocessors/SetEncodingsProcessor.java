package messageprocessors;

import framebuffer.ServerFramebuffer;
import messages.SetEncodingsMessage;

import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetEncodingsProcessor extends MessageProcessor
{
    public SetEncodingsProcessor( SetEncodingsMessage message, ServerFramebuffer framebuffer )
    {
        super( message, framebuffer );
    }


    @Override
    protected void reactBody() throws IOException
    {
        // Do nothing?
    }
}
