package messageprocessors;

import framebuffer.ServerFramebuffer;
import messages.KeyEventMessage;

import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class KeyEventProcessor extends MessageProcessor
{
    public KeyEventProcessor( KeyEventMessage message, ServerFramebuffer framebuffer )
    {
        super( message, framebuffer );
    }


    @Override
    protected void reactBody() throws IOException
    {
        KeyEventMessage message = (KeyEventMessage) this.message;

        if ( message.pressed )
        {
            ( (ServerFramebuffer)this.framebuffer ).pressKey( message.getKeyCode() );
        }
    }
}
