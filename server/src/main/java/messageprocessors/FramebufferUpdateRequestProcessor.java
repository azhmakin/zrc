package messageprocessors;

import framebuffer.ServerFramebuffer;
import messages.FramebufferUpdateMessage;
import messages.FramebufferUpdateRequestMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class FramebufferUpdateRequestProcessor extends MessageProcessor
{
    private DataOutputStream outputStream;
    private long sendingDuration = 0;
    private long lastSent = 0;


    public FramebufferUpdateRequestProcessor( FramebufferUpdateRequestMessage message,
                                              ServerFramebuffer         framebuffer,
                                              DataOutputStream          outputStream )
    {
        super( message, framebuffer );

        this.outputStream = outputStream;
    }


    @Override
    protected void reactBody()
            throws IOException
    {
        long startMoment = System.currentTimeMillis();

        FramebufferUpdateRequestMessage message = (FramebufferUpdateRequestMessage) this.message;

        this.sendFramebufferRegion( message.x, message.y, message.width, message.height );

        this.lastSent = System.currentTimeMillis();
        this.sendingDuration = ( this.sendingDuration + (this.lastSent - startMoment) ) / 2;
    }


    private void sendFramebufferRegion( int x, int y, int width, int height )
            throws IOException
    {
        FramebufferUpdateMessage framebufferUpdate = new FramebufferUpdateMessage();

        framebufferUpdate.framebuffer = framebuffer;
        framebufferUpdate.regionList = new ArrayList<FramebufferUpdateMessage.Region>(1);

        FramebufferUpdateMessage.Region region = framebufferUpdate.newRegion( x, y, width, height );

        framebufferUpdate.regionList.add( region );

        framebufferUpdate.write( this.outputStream );
    }


    public void sendWholeFramebuffer()
            throws IOException
    {
        long startMoment = System.currentTimeMillis();

        this.sendFramebufferRegion( 0, 0, this.framebuffer.getWidth(), this.framebuffer.getHeight() );

        this.lastSent = System.currentTimeMillis();
        this.sendingDuration = ( this.sendingDuration + (this.lastSent - startMoment) ) / 2;
    }


    public long getSendingDuration()
    {
        return this.sendingDuration;
    }


    public long getLastSent()
    {
        return this.lastSent;
    }
}
