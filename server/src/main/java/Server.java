import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class Server
{
    private static frmControlPanel controlPanel;
    public static boolean bitRunning;


    public static void main( String[] args )
    {
        JFrame frame = new JFrame( "ZRC Server - Andrey Zhmakin" );
        controlPanel = new frmControlPanel();
        frame.setContentPane( controlPanel.getMainPanel() );
        frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        frame.pack();
        frame.setSize( 600, 450 );
        frame.setLocationRelativeTo(null);
        frame.setResizable( false );
        frame.setVisible( true );

        frame.addWindowListener( new WindowListener()
        {
            @Override
            public void windowClosing( WindowEvent e )
            {
                log( "Closing server..." );
            }

            @Override public void windowOpened      ( WindowEvent e ) { }
            @Override public void windowClosed      ( WindowEvent e ) { }
            @Override public void windowIconified   ( WindowEvent e ) { }
            @Override public void windowDeiconified ( WindowEvent e ) { }
            @Override public void windowActivated   ( WindowEvent e ) { }
            @Override public void windowDeactivated ( WindowEvent e ) { }
        } );

        start();
    }


    public static synchronized void log( String s )
    {
        controlPanel.log_print( s );
    }


    public static void start()
    {
        try
        {
            int port = RCUtil.DEFAULT_LISTENING_PORT;
            ServerSocket serverSocket = new ServerSocket( port );
            log( "Listening at port " + port + "\n" );

            bitRunning = true;

            while ( bitRunning )
            {
                Socket socket = serverSocket.accept();

                log( "Connection from " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort() + "\n" );

                Connection connection = new Connection( socket );
                Thread connectionThread = new Thread( connection );
                connectionThread.start();
            }

            log( "Done!\n" );
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }
}
