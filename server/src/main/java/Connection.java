import framebuffer.ServerFramebuffer;
import messageprocessors.*;
import messages.*;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class Connection implements Runnable
{
    private DataOutputStream outputStream;

    private ServerFramebuffer framebuffer;

    private MessageProcessor[] messageProcessor;

    private Socket socket;

    private FramebufferUpdateRequestProcessor framebufferUpdateRequestProcessor;

    private boolean bitMessageLoop;

    /**
     * Two supported security types:
     *   1 - None
     *   2 - VNC Authentication
     */
    private final byte[] SECURITY_TYPE_REQUEST = { 2, 1, 2 };


    public Connection( Socket socket )
    {
        this.socket = socket;
    }


    @Override
    public void run()
    {
        try
        {
            DataInputStream inputStream = new DataInputStream( this.socket.getInputStream() );

            this.outputStream = new DataOutputStream( this.socket.getOutputStream() );

            this.outputStream.write( RCUtil.PROTOCOL_VERSION );
            //System.out.println("PROTOCOL_VERSION Sent!");

            String negotiatedProtocolVersionStr = inputStream.readLine();
            //System.out.println( "negotiatedProtocolVersion = \'" + negotiatedProtocolVersionStr + "\'" );

            byte[] negotiatedProtocolVersion = negotiatedProtocolVersionStr.getBytes();

            if ( !RCUtil.compareProtocolVersion( RCUtil.PROTOCOL_VERSION, negotiatedProtocolVersion ) )
            {
                this.outputStream.writeByte( 0 );
                String message = "Can\'t negotiate protocol version!";

                Server.log( message + "\n" );

                this.outputStream.writeInt( message.length() );
                this.outputStream.write( message.getBytes() );

                this.outputStream.close();
                socket.close();
                System.exit( 0 );
            }

            this.outputStream.write( SECURITY_TYPE_REQUEST );
            int negotiatedSecurityType = inputStream.readByte();
            //System.out.println( "negotiatedSecurityType = " + negotiatedSecurityType );

            // Send SecurityResult messageProcessor
            switch ( negotiatedSecurityType )
            {
                // Failure
                case 0:
                    Server.log( "Can\'t negotiate security type!\n" );
                    this.socket.close();
                    return;

                    // None
                case 1:
                    // 0 - Success
                    //outputStream.writeInt( 0 );
                    break;

                // VNC Authentication
                case 2:
                    // 0 - Failure
                    Server.log( "VNA Authentication is not implemented!\n" );
                    this.outputStream.writeInt( 1 );
                    this.outputStream.close();
                    assert false : "Not implemented!";
                    // TODO: Implement me!
            }

            // Read ClientInit
            byte clientInit = inputStream.readByte();
            //System.out.println( "clientInit = " + clientInit );

            this.framebuffer = new ServerFramebuffer();
            this.initMessageProcessor();

            // Send ServerInit
            this.outputStream.writeShort( this.framebuffer.getWidth() );
            this.outputStream.writeShort( this.framebuffer.getHeight() );
            this.outputStream.write( this.framebuffer.getPixelFormat(), 0, 16 );

            byte[] framebufferName = new byte[] { 'A', 'n', 'd', 'r', 'e', 'y' };

            this.outputStream.writeInt( framebufferName.length );
            this.outputStream.write( framebufferName );

            //System.out.println( "ServerInit sent!" );

            Server.log( "Connection with "
                    + this.socket.getInetAddress().getHostName()
                    + " (" + this.socket.getInetAddress().getHostAddress() + ":" + this.socket.getPort() + ")"
                    + " established!\n" );

            this.bitMessageLoop = true;

            // Message loop!
            while ( this.bitMessageLoop )
            {
                try
                {
                    byte id = inputStream.readByte();

                    //System.out.println( "id = " + id );

                    MessageProcessor processor = this.getMessageProcessor( id );

                    if ( processor == null )
                    {
                        assert false : id + " messageProcessor not supported!";
                    }

                    processor.react( inputStream );

                    if ( System.currentTimeMillis() - this.framebufferUpdateRequestProcessor.getLastSent()
                            > this.framebufferUpdateRequestProcessor.getSendingDuration() * 2 )
                    {
                        this.framebufferUpdateRequestProcessor.sendWholeFramebuffer();
                    }
                }
                catch ( IOException e )
                {
                    this.bitMessageLoop = false;
                }
            }

            Server.log( "Connection with "
                    + this.socket.getInetAddress().getHostName()
                    + " (" + this.socket.getInetAddress().getHostAddress() + ":" + this.socket.getPort() + ")"
                    + " closed!\n" );
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        catch ( AWTException e )
        {
            e.printStackTrace();
        }
    }


    private MessageProcessor getMessageProcessor( byte id )
    {
        return ( id < 0 || id >= this.messageProcessor.length ) ? null : this.messageProcessor[id];
    }


    private void initMessageProcessor()
    {
        java.util.List<MessageProcessor> processorList = new LinkedList<MessageProcessor>();

        this.framebufferUpdateRequestProcessor
                = new FramebufferUpdateRequestProcessor( new FramebufferUpdateRequestMessage(),
                                                         this.framebuffer,
                                                         this.outputStream );

        processorList.add( this.framebufferUpdateRequestProcessor );

        processorList.add( new KeyEventProcessor( new KeyEventMessage(), this.framebuffer ) );
        processorList.add( new PointerEventProcessor( new PointerEventMessage(), this.framebuffer ) );
        processorList.add( new SetEncodingsProcessor( new SetEncodingsMessage(), this.framebuffer ) );

        processorList.add( new SetPixelFormatProcessor( new SetPixelFormatMessage(),
                                                        this.framebuffer,
                                                        this.outputStream ) );

        this.messageProcessor = new MessageProcessor[16];

        for ( MessageProcessor processor : processorList )
        {
            this.messageProcessor[ processor.getID() ] = processor;
        }
    }
}
