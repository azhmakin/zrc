import javax.swing.*;
import java.awt.*;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public class frmControlPanel
{
    private JPanel pnlMain;
    private JTextPane txpLog;

    public frmControlPanel()
    {
        ;
    }


    public synchronized void log_print( String s )
    {
        String text = this.txpLog.getText();

        text += s;

        this.txpLog.setText( text );
    }


    public Container getMainPanel()
    {
        return this.pnlMain;
    }
}
