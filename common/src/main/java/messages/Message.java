package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public interface Message
{
    public byte getID();
    public int read( DataInputStream stream ) throws IOException;
    public int write( DataOutputStream stream ) throws IOException;
}
