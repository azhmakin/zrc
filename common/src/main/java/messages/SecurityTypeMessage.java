package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SecurityTypeMessage
{
    public byte[] type;


    public int read( DataInputStream stream ) throws IOException
    {
        byte numOfTypes = stream.readByte();

        this.type = new byte[numOfTypes];

        for ( int i = 0; i < numOfTypes; i++ )
        {
            this.type[i] = stream.readByte();
        }

        return 0;
    }


    public int write( DataOutputStream stream ) throws IOException
    {
        assert false : "Not implemented!";

        return 0;
    }
}
