package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetColourMapEntriesMessage implements Message
{
    public short firstColor;
    public List<Entry> entryList;

    public static class Entry
    {
        public short r;
        public short g;
        public short b;


        public Entry( short r, short g, short b )
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }


        public Entry( DataInputStream stream )
                throws IOException
        {
            this.r = stream.readShort();
            this.g = stream.readShort();
            this.b = stream.readShort();
        }


        public void write( DataOutputStream stream ) throws IOException
        {
            stream.writeShort( this.r );
            stream.writeShort( this.g );
            stream.writeShort( this.b );
        }


        public int toRGB()
        {
            //return Color.RED.getRGB();

            int rr = ( r >> 8 ) & 0x0000FF;
            int rg = (int)g & 0x00FF00;
            int rb = ( (int)b << 8 ) & 0xFF0000;

            return rr | rg | rb | 0xFF000000;
        }
    }


    @Override
    public byte getID()
    {
        return ServerToClientMessage.SET_COLOUR_MAP_ENTRIES;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
        // Skip padding
        stream.readByte();

        this.firstColor = stream.readShort();
        assert this.firstColor == 0;

        short numOfColors = stream.readShort();

        this.entryList = new ArrayList<Entry>( numOfColors );

        for ( int i = numOfColors; --i >= 0; )
        {
            Entry entry = new Entry( stream );

            this.entryList.add( entry );
        }

        return 0;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        stream.writeByte( ServerToClientMessage.SET_COLOUR_MAP_ENTRIES );

        // Pass padding
        stream.writeByte( 0 );

        stream.writeShort( 0 );
        stream.writeShort( this.entryList.size() );

        for ( Entry entry : this.entryList )
        {
            entry.write( stream );
        }

        return 0;
    }
}
