package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class KeyEventMessage implements Message
{

    public boolean pressed;
    public int keysym;


    @Override
    public byte getID()
    {
        return ClientToServerMessage.KEY_EVENT;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
        this.pressed = stream.readByte() != 0;

        // skip padding
        stream.readShort();

        this.keysym = stream.readInt();

        return 0;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        stream.writeByte( this.getID() );

        stream.writeByte( this.pressed ? 1 : 0 );

        // Pass padding
        stream.writeShort( 0 );

        stream.writeInt( this.keysym );

        return 0;
    }


    public int getKeyCode()
    {
        return KeyConverter.getInstance().from( this.keysym );
    }


    public void setKey( int keyCode )
    {
        this.keysym = KeyConverter.getInstance().to( keyCode );
    }
}
