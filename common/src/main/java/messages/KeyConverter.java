package messages;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class KeyConverter
{
    private static KeyConverter instance = new KeyConverter();


    public static KeyConverter getInstance()
    {
        return instance;
    }


    private Map<Integer, Integer> toTable;
    private Map<Integer, Integer> fromTable;


    private KeyConverter()
    {
        this.fromTable = new HashMap<Integer, Integer>();

        this.fromTable.put( 0xff08, KeyEvent.VK_BACK_SPACE );
        this.fromTable.put( 0xff09, KeyEvent.VK_TAB        );
        this.fromTable.put( 0xff0d, KeyEvent.VK_ENTER      );
        this.fromTable.put( 0xff1b, KeyEvent.VK_ESCAPE     );
        this.fromTable.put( 0xff63, KeyEvent.VK_INSERT     );
        this.fromTable.put( 0xffff, KeyEvent.VK_DELETE     );
        this.fromTable.put( 0xff50, KeyEvent.VK_HOME       );
        this.fromTable.put( 0xff57, KeyEvent.VK_END        );
        this.fromTable.put( 0xff55, KeyEvent.VK_PAGE_UP    );
        this.fromTable.put( 0xff56, KeyEvent.VK_PAGE_DOWN  );
        this.fromTable.put( 0xff51, KeyEvent.VK_LEFT       );
        this.fromTable.put( 0xff52, KeyEvent.VK_UP         );
        this.fromTable.put( 0xff53, KeyEvent.VK_RIGHT      );
        this.fromTable.put( 0xff54, KeyEvent.VK_DOWN       );
        this.fromTable.put( 0xff54, KeyEvent.VK_DOWN       );
        this.fromTable.put( 0x0020, KeyEvent.VK_SPACE      );

        for ( int i = KeyEvent.VK_A; i <= KeyEvent.VK_Z; i++ )
        {
            this.fromTable.put( i - KeyEvent.VK_A + 0x0061, i );
        }

        for ( int i = KeyEvent.VK_0; i <= KeyEvent.VK_9; i++ )
        {
            this.fromTable.put( i - KeyEvent.VK_0 + 0x0030, i );
        }

        this.toTable = new HashMap<Integer, Integer>( this.fromTable.size() );

        for ( Map.Entry<Integer, Integer> entry : this.fromTable.entrySet() )
        {
            this.toTable.put( entry.getValue(), entry.getKey() );
        }
    }


    public int to( int keyCode )
    {
        Integer result = this.toTable.get( keyCode );

        return result != null ? result : -1;
    }


    public int from( int keysym )
    {
        Integer result = this.fromTable.get( keysym );

        return result != null ? result : -1;
    }
}
