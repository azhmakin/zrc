package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetEncodingsMessage implements Message
{
    short numberOfEncodings;
    List<Integer> encodingList;


    @Override
    public byte getID()
    {
        return ClientToServerMessage.SET_ENCODINGS;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
         // Skip padding
        stream.readByte();

        this.numberOfEncodings = stream.readShort();

        this.encodingList = new ArrayList<Integer>( this.numberOfEncodings );

        for ( int i = this.numberOfEncodings; --i >= 0; )
        {
            int encoding = stream.readInt();

            this.encodingList.add( encoding );
        }

        return 0;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        return 0;
    }
}
