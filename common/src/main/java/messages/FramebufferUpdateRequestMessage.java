package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class FramebufferUpdateRequestMessage implements Message
{
    public boolean incremental;
    public int x;
    public int y;
    public int width;
    public int height;


    @Override
    public byte getID()
    {
        return ClientToServerMessage.FRAMEBUFFER_UPDATE_REQUEST;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
        this.incremental = stream.readByte() != 0;

        this.x = stream.readShort();
        this.y = stream.readShort();

        this.width = stream.readShort();
        this.height = stream.readShort();

        return 0;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        stream.writeByte( this.getID() );

        stream.writeByte( this.incremental ? 1 : 0 );

        stream.writeShort( this.x );
        stream.writeShort( this.y );

        stream.writeShort( this.width );
        stream.writeShort( this.height );

        return 0;
    }
}
