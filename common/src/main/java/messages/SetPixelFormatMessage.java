package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class SetPixelFormatMessage implements Message
{
    byte bitsPerPixel;
    byte depth;
    byte bigEndianFlag;
    byte trueColorFlag;

    short redMax;
    short greenMax;
    short blueMax;

    byte redShift;
    byte greenShift;
    byte blueShift;


    @Override
    public byte getID()
    {
        return ClientToServerMessage.SET_PIXEL_FORMAT;
    }


    @Override
    public int read( DataInputStream stream )
            throws IOException
    {
        // Skip padding
        stream.readByte();
        stream.readByte();
        stream.readByte();

        this.bitsPerPixel = stream.readByte();
        this.depth = stream.readByte();
        this.bigEndianFlag = stream.readByte();
        this.trueColorFlag = stream.readByte();

        this.redMax = stream.readShort();
        this.greenMax = stream.readShort();
        this.blueMax = stream.readShort();

        this.redShift = stream.readByte();
        this.greenShift = stream.readByte();
        this.blueShift = stream.readByte();

        /*
        System.out.println( "bitsPerPixel = " + bitsPerPixel );
        System.out.println( "depth = " + depth );
        System.out.println( "bigEndianFlag = " + bigEndianFlag );
        System.out.println( "trueColorFlag = " + trueColorFlag );
        System.out.println( "redMax = " + redMax );
        */

        // Skip padding
        stream.readByte();
        stream.readByte();
        stream.readByte();

        return 0;
    }

    @Override
    public int write( DataOutputStream stream )
            throws IOException
    {
        stream.writeByte( this.getID() );

        // Send padding
        stream.writeByte( 0 );
        stream.writeByte( 0 );
        stream.writeByte( 0 );

        stream.writeByte( this.bitsPerPixel );
        stream.writeByte( this.depth );
        stream.writeByte( this.bigEndianFlag );
        stream.writeByte( this.trueColorFlag );

        stream.writeShort( this.redMax );
        stream.writeShort( this.greenMax );
        stream.writeShort( this.blueMax );

        stream.writeByte( this.redShift );
        stream.writeByte( this.greenShift );
        stream.writeByte( this.blueShift );

        // Send padding
        stream.writeByte( 0 );
        stream.writeByte( 0 );
        stream.writeByte( 0 );

        return 0;
    }
}
