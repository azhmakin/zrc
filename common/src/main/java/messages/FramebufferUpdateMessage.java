package messages;

import framebuffer.ClientFramebuffer;
import framebuffer.Framebuffer;
import framebuffer.ServerFramebuffer;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class FramebufferUpdateMessage implements Message
{
    public Framebuffer framebuffer;
    public short numberOfRectangles;
    public java.util.List<Region> regionList;


    @Override
    public byte getID()
    {
        return ServerToClientMessage.FRAMEBUFFER_UPDATE;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        stream.writeByte( ServerToClientMessage.FRAMEBUFFER_UPDATE );

        // Pass padding
        stream.writeByte( 0 );

        // TODO : Consider other number of rectangles!
        this.numberOfRectangles = (short) this.regionList.size();
        stream.writeShort( this.numberOfRectangles );

        for ( Region region : this.regionList )
        {
            region.write( stream );
        }

        return 0;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
        // Skip padding
        stream.readByte();

        this.numberOfRectangles = stream.readShort();

        Region region = new Region();

        for ( int i = 0; i < this.numberOfRectangles; i++ )
        {
            region.read( stream );
        }

        return 0;
    }


    public Region newRegion( int x, int y, int width, int height )
    {
        Region region = new Region();

        region.x = x;
        region.y = y;
        region.width = width;
        region.height = height;

        return region;
    }


    public final class Region extends Rectangle
    {
        public int write( DataOutputStream stream )
                throws IOException
        {
            stream.writeShort( this.x );
            stream.writeShort( this.y );
            stream.writeShort( this.width );
            stream.writeShort( this.height );

            // Raw encoding
            stream.writeInt( 0 );

            ((ServerFramebuffer) framebuffer).write( this, stream );

            return 0;
        }


        public int read( DataInputStream stream )
                throws IOException
        {
            this.x      = stream.readShort();
            this.y      = stream.readShort();
            this.width  = stream.readShort();
            this.height = stream.readShort();

            // Raw encoding
            int encoding = stream.readInt();
            assert encoding == 0;

            ClientFramebuffer framebuffer = (ClientFramebuffer) FramebufferUpdateMessage.this.framebuffer;

            framebuffer.read( this, stream );

            return 0;
        }
    }
}
