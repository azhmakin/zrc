package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class PointerEventMessage implements Message
{
    public byte button;
    public int x;
    public int y;


    @Override
    public byte getID()
    {
        return ClientToServerMessage.POINTER_EVENT;
    }


    @Override
    public int read( DataInputStream stream ) throws IOException
    {
        this.button = stream.readByte();

        this.x = stream.readShort();
        this.y = stream.readShort();

        return 0;
    }


    @Override
    public int write( DataOutputStream stream ) throws IOException
    {
        stream.writeByte( this.getID() );

        stream.writeByte( this.button );

        stream.writeShort( this.x );
        stream.writeShort( this.y );

        return 0;
    }
}
