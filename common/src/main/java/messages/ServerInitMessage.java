package messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class ServerInitMessage
{
    public short framebufferWidth;
    public short framebufferHeight;
    byte[] pixelFormat;
    byte[] name;


    public int read( DataInputStream stream ) throws IOException
    {
        int bytesRead;

        this.framebufferWidth = stream.readShort();
        this.framebufferHeight = stream.readShort();

        //System.out.println( "framebufferWidth = " + framebufferWidth );
        //System.out.println( "framebufferHeight = " + framebufferHeight );

        this.pixelFormat = new byte[16];
        bytesRead = stream.read( this.pixelFormat, 0, 16 );

        if ( bytesRead != 16 )
        {
            return 1;
        }

        int nameLength = stream.readInt();

        this.name = new byte[nameLength];
        bytesRead = stream.read( this.name, 0, nameLength );

        if ( bytesRead != nameLength )
        {
            return 1;
        }

        return 0;
    }


    public int write( DataOutputStream stream ) throws IOException
    {
        assert false : "Not implemented!";

        return 0;
    }
}
