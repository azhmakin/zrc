package messages;


public interface ServerToClientMessage
{
    byte FRAMEBUFFER_UPDATE = 0;
    byte SET_COLOUR_MAP_ENTRIES = 1;
}
