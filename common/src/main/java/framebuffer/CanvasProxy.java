package framebuffer;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public interface CanvasProxy
{
    void plot( int x, int y, int color );

    void flush();
}
