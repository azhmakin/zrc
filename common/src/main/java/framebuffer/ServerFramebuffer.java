package framebuffer;

import messages.FramebufferUpdateMessage;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class ServerFramebuffer extends Framebuffer
{
    private Robot robot;

    public ServerFramebuffer()
            throws AWTException
    {
        super();

        this.robot = new Robot();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.width = (int) screenSize.getWidth();
        this.height = (int) screenSize.getHeight();

        this.name = "FrameBufferA-" + this.width + "x" + this.height;

        this.pixelFormat = new byte[16];

        this.pixelFormat[0] = 8; // bits-per-pixel
        this.pixelFormat[1] = 8; // depth

        this.pixelFormat[2] = 0; // big-endian-flag

        this.pixelFormat[3] = 1; // true-colour-flag

        // red-max
        this.pixelFormat[4] = 0;
        this.pixelFormat[5] = -1;

        // green-max
        this.pixelFormat[6] = 0;
        this.pixelFormat[7] = -1;

        // red-max
        this.pixelFormat[8] = 0;
        this.pixelFormat[9] = -1;

        this.pixelFormat[10] = 0;
        this.pixelFormat[11] = 0;
        this.pixelFormat[12] = 0;
    }


    private int[] rgbBuffer = new int[1];
    private byte[] buffer;


    public void write( FramebufferUpdateMessage.Region rectangle, DataOutputStream stream )
            throws IOException
    {
        BufferedImage screenShot = this.robot.createScreenCapture( rectangle );

        int minLength = rectangle.width * rectangle.height;

        if ( this.rgbBuffer.length < minLength )
        {
            this.rgbBuffer  = new int[minLength];
            this.buffer     = new byte[minLength];
        }

        screenShot.getRGB( 0, 0, rectangle.width, rectangle.height, this.rgbBuffer, 0, rectangle.width );

        for ( int index = 0; index < minLength; index++ )
        {
            int rgb = this.rgbBuffer[index];

            int r = (rgb      ) & 0xFF;
            int g = (rgb >>  8) & 0xFF;
            int b = (rgb >> 16) & 0xFF;

            this.buffer[index] = (byte)( (r + g + b) / 3 );
        }

        stream.write( this.buffer, 0, minLength );
    }


    public void mouseTo( int x, int y )
    {
        this.robot.mouseMove( x, y );
    }

    public void mouseLeftButton( boolean press )
    {
        if ( press )
            this.robot.mousePress( InputEvent.BUTTON1_DOWN_MASK );
        else
            this.robot.mouseRelease( InputEvent.BUTTON1_DOWN_MASK );
    }

    public void mouseRightButton( boolean press )
    {
        if ( press )
            this.robot.mousePress( InputEvent.BUTTON2_DOWN_MASK );
        else
            this.robot.mouseRelease( InputEvent.BUTTON2_DOWN_MASK );
    }


    public void pressKey( int keyCode )
    {
        try
        {
            this.robot.keyPress( keyCode );
        }
        catch ( IllegalArgumentException e )
        {
            e.printStackTrace();
        }
    }
}
