package framebuffer;

import messages.FramebufferUpdateMessage;

import java.awt.*;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public final class ClientFramebuffer extends Framebuffer
{
    private CanvasProxy canvasProxy;
    public int[] colorTable;


    public ClientFramebuffer( CanvasProxy canvasProxy ) throws AWTException
    {
        super();

        this.canvasProxy = canvasProxy;
        this.colorTable = new int[256];
    }


    public void read( FramebufferUpdateMessage.Region rectangle, DataInputStream stream )
            throws IOException
    {
        byte[] buffer = new byte[rectangle.width];

        for ( int y = 0; y < rectangle.height; y++ )
        {
            int bytesRead = 0;
            int x = 0;

            while ( bytesRead < rectangle.width )
            {
                bytesRead += stream.read( buffer, bytesRead, rectangle.width - bytesRead );

                for ( ; x < bytesRead; x++ )
                {
                    int color = buffer[x];

                    color &= 0xFF;

                    int rgb = this.colorTable[color];

                    this.canvasProxy.plot( rectangle.x + x, rectangle.y + y, rgb );
                }
            }

            this.canvasProxy.flush();
        }
    }


    public void setSize( int width, int height )
    {
        this.width = width;
        this.height = height;
    }
}
