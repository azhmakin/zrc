package framebuffer;

import java.awt.*;

/**
 * Created by Andrey Zhmakin, 2013.
 */
public abstract class Framebuffer
{
    protected String name;
    protected int width;
    protected int height;
    protected byte[] pixelFormat;


    public Framebuffer() throws AWTException
    {

    }


    public int getWidth()
    {
        return this.width;
    }


    public int getHeight()
    {
        return this.height;
    }


    public String getName()
    {
        return name;
    }


    public byte[] getPixelFormat()
    {
        return this.pixelFormat;
    }
}
