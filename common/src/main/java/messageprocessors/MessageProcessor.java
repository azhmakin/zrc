package messageprocessors;

import framebuffer.Framebuffer;
import messages.Message;

import java.io.DataInputStream;
import java.io.IOException;

public abstract class MessageProcessor
{
    protected Message message;
    protected Framebuffer framebuffer;


    public MessageProcessor( Message message, Framebuffer framebuffer )
    {
        this.message = message;
        this.framebuffer = framebuffer;
    }


    public final byte getID()
    {
        return this.message.getID();
    }


    public final void read( DataInputStream stream )
            throws IOException
    {
        this.message.read( stream );
    }


    public final void react( DataInputStream stream ) throws IOException
    {
        this.read( stream );
        this.reactBody();
    }


    protected abstract void reactBody() throws IOException;
}
