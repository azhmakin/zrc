/**
 * Created by Andrey Zhmakin, 2013.
 */
public class RCUtil
{
    public final static byte[] PROTOCOL_VERSION = { 'R', 'F', 'B', ' ', '0', '0', '3', '.', '0', '0', '7', '\n' };

    public final static int DEFAULT_LISTENING_PORT = 5900;

    public static boolean compareProtocolVersion( byte[] a, byte[] b )
    {
        boolean versionNegotiated = true;

        for ( int i = Math.min( a.length, b.length );
              --i >= 0 && versionNegotiated; )
        {
            versionNegotiated = a[i] == b[i];
        }

        return versionNegotiated;
    }
}
